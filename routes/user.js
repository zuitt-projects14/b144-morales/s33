const express = require("express");
const router = express.Router();
const auth = require("../auth")

const userController = require("../controllers/user")
//Route for checking if the user's email is already exist in the database
router.post("/checkEmail", (req, res)=>{
	userController.checkEmailExists(req.body).then(result => res.send(result))
})

//Routes for User Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result));
})


//Routes for authenticating a user

router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result))
})


//Activity starts here

/*router.post("/details", (req, res) => {
	
	userController.getProfile(req.body._id).then(result => res.send(result));
})*/


/*router.get("/:id/details", (req, res) => {
	//get all task function
	//call the task controller
	userController.getProfile(req.params.id, req.body).then(result => res.send(result));
})*/





/*router.post("/:id/details", (req, res) => {
	//get all task function
	//call the task controller
	userController.postProfile(req.params.id, req.body).then(result => res.send(result));
})*/




/*router.get("details", (req, res)=>{
	userController.getProfile(req.body).then(result => res.send(result));
})*/

//Activity Solution

router.get("/details", auth.verify, (req, res) => {

       	const userData = auth.decode(req.headers.authorization)	
	//get all task function
	//call the task controller
	userController.getProfile({userId: userData.id}).then(result => res.send(result));
})

















module.exports = router;